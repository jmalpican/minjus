package com.codenotfound.mapper;

import com.codenotfound.bean.Persona;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.jdbc.core.RowMapper;

import java.io.ByteArrayInputStream;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonaRowMapper implements RowMapper<Persona> {
    @Override
    public Persona mapRow(ResultSet rs, int rowNum) throws SQLException {
        Persona persona = new Persona();

        persona.setPersonal_id(rs.getString("personal_id"));
        persona.setNombres(rs.getString("nombres"));
        persona.setApellido_paterno(rs.getString("apellido_paterno"));
        persona.setApellido_materno(rs.getString("apellido_materno"));
        persona.setCorreo_electronico(rs.getString("correo_electronico"));
        persona.setNumero_telefonico(rs.getString("celular"));
        persona.setFotos(rs.getBytes("foto"));
        persona.setImage(getFoto(persona));
//        persona.setImage(new DefaultStreamedContent(new ByteArrayInputStream(rs.getBytes("foto")), "image/png"));
        persona.setSede(rs.getString("nombreSede"));
        persona.setEstado(rs.getString("estadoId"));
        persona.setNombre_estado(rs.getString("nombreestado"));
        persona.setDireccion_distrital(rs.getString("nombredistritojudicial"));
        return persona;
    }

    private StreamedContent getFoto(Persona persona){
//        if ("021820".equals(persona.getPersonal_id()) ){
            return new DefaultStreamedContent(new ByteArrayInputStream(persona.getFotos()), "image/png");
//        }else{
//            return null;
//        }
    }
}
