package com.codenotfound.mapper;

import com.codenotfound.bean.Departament;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DepartamentRowMapper implements RowMapper<Departament> {
    @Override
    public Departament mapRow(ResultSet rs, int rowNum) throws SQLException {
        Departament departament = new Departament();

        departament.setCoddpto(rs.getString("coddpto"));
        departament.setNombre(rs.getString("nombre"));

        return departament;
    }
}
