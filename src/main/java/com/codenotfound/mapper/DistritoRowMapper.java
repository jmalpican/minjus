package com.codenotfound.mapper;

import com.codenotfound.bean.Distrito;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DistritoRowMapper implements RowMapper<Distrito> {
    @Override
    public Distrito mapRow(ResultSet rs, int rowNum) throws SQLException {
        Distrito distrito =  new Distrito();

        distrito.setCoddpto(rs.getString("coddpto"));
        distrito.setCodprov(rs.getString("codprov"));
        distrito.setCoddist(rs.getString("coddist"));
        distrito.setNombre(rs.getString("nombre"));

        return distrito;
    }
}
