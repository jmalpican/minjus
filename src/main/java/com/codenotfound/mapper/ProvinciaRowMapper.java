package com.codenotfound.mapper;

import com.codenotfound.bean.Provincia;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProvinciaRowMapper implements RowMapper<Provincia> {
    @Override
    public Provincia mapRow(ResultSet rs, int rowNum) throws SQLException {
        Provincia provincia =  new Provincia();

        provincia.setCoddpto(rs.getString("coddpto"));
        provincia.setCodprov(rs.getString("codprov"));
        provincia.setNombre(rs.getString("nombre"));

        return provincia;
    }
}
