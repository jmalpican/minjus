package com.codenotfound.mapper;

import com.codenotfound.bean.Sede;
import com.codenotfound.bean.SedeVW;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class SedeVWRowMapper implements RowMapper<SedeVW> {
    @Override
    public SedeVW mapRow(ResultSet rs, int rowNum) throws SQLException {
        SedeVW sede = new SedeVW();

        sede.setSede_id(rs.getString("sede_id"));
        sede.setNombre(rs.getString("nombre"));
        sede.setDistritojudicial_id(rs.getString("distritojudicial_id"));
        sede.setCoddpto(rs.getString("coddpto"));
        sede.setCodprov(rs.getString("codprov"));
        sede.setCoddist(rs.getString("coddist"));
        return sede;
    }
}
