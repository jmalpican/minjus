package com.codenotfound.mapper;

import com.codenotfound.bean.Sede;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class SedeRowMapper implements RowMapper<Sede> {
    @Override
    public Sede mapRow(ResultSet rs, int rowNum) throws SQLException {
        Sede sede = new Sede();

        sede.setSede_id(rs.getString("sede_id"));
        sede.setDireccion(rs.getString("direccion"));
        sede.setTelefono(rs.getString("telefono"));
        sede.setCorreo(rs.getString("correo"));
        sede.setCoddpto(rs.getString("coddpto"));
        sede.setCodprov(rs.getString("codprov"));
        sede.setCoddist(rs.getString("coddist"));
        sede.setPersonal_id(rs.getString("personal_id"));
        sede.setLongitud(rs.getString("longitud"));
        sede.setLatitud(rs.getString("latitud"));
        sede.setActivo(rs.getString("activo"));
        sede.setAuditUsuarioCreacion(rs.getString("auditUsuarioCreacion"));
        sede.setAuditFechaCreacion(rs.getDate("auditFechaCreacion"));
        sede.setAuditUsuarioModifica(rs.getString("auditUsuarioModifica"));
        sede.setAuditFechaCreacion(rs.getDate("auditFechaModifica"));
        sede.setLongitudDpto(rs.getString("longitudDpto"));
        sede.setLatitudDpto(rs.getString("latitudDpto"));
        sede.setNombreEstado(rs.getString("nombreEstado"));
        return sede;
    }
}
