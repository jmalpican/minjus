package com.codenotfound.primefaces;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import com.codenotfound.bean.*;
import com.codenotfound.services.DepartamentService;
import com.codenotfound.services.DistritoService;
import com.codenotfound.services.PersonaService;
import com.codenotfound.services.ProvinciaService;
import com.codenotfound.services.SedeService;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.map.OverlaySelectEvent;
import org.springframework.beans.factory.annotation.Autowired;

import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

@Named
@ViewScoped
public class DepartamentsView implements Serializable {

  private static final long serialVersionUID = 1L;

  @Autowired
  private DepartamentService departamentService;
  @Autowired
  private PersonaService personaService;
  @Autowired
  private ProvinciaService provinciaService;
  @Autowired
  private DistritoService distritoService;
  @Autowired
  private SedeService sedeService;

  private List<Departament> departaments;
  private List<Provincia> provincias;
  private List<Distrito> distritos;
  private List<Persona> personas;
  private List<Sede> sedes;
  private List<SedeVW> sedesVW;
  private String idDepartamento;
  private String idProvincia;
  private String idDistrito;
  private String idSede;
  private MapModel simpleModel;
  private Sede sede;

  private Persona persona;

  private String nombres;

  public String getLongitud() {
    return longitud;
  }

  public void setLongitud(String longitud) {
    this.longitud = longitud;
  }

  public String getLatitud() {
    return latitud;
  }

  public void setLatitud(String latitud) {
    this.latitud = latitud;
  }

  private String longitud;

  private String latitud;

  private double longitudSuma;

  private int contador;

  private double latitudSuma;

  private String apellidopaterno;
  private String apellidomaterno;

  private Marker marker;

  public String getZoom() {
    return zoom;
  }

  public void setZoom(String zoom) {
    this.zoom = zoom;
  }

  private String zoom;

  public String getIdEstado() {
    return idEstado;
  }

  public void setIdEstado(String idEstado) {
    this.idEstado = idEstado;
  }

  private String idEstado;

  public String getIdDistrito() {
    return idDistrito;
  }

  public void setIdDistrito(String idDistrito) {
    this.idDistrito = idDistrito;
  }

  public String getNombres() {
    return nombres;
  }

  public void setNombres(String nombres) {
    this.nombres = nombres;
  }

  public String getApellidopaterno() {
    return apellidopaterno;
  }

  public void setApellidopaterno(String apellidopaterno) {
    this.apellidopaterno = apellidopaterno;
  }

  public String getApellidomaterno() {
    return apellidomaterno;
  }

  public void setApellidomaterno(String apellidomaterno) {
    this.apellidomaterno = apellidomaterno;
  }


  public String getIdDepartamento() {
    return idDepartamento;
  }

  public void setIdDepartamento(String idDepartamento) {
    this.idDepartamento = idDepartamento;
  }

  public String getIdProvincia() {
    return idProvincia;
  }

  public void setIdProvincia(String idProvincia) {
    this.idProvincia = idProvincia;
  }

  @PostConstruct
  public void init() {

    personas = personaService.allPersona();
    departaments = departamentService.allDepartament();
    provincias = new ArrayList<Provincia>();
    sedes = sedeService.allSedes();
    sede = new Sede();
    persona = new Persona();
    longitud = "-10.08367";
    latitud = "-75.80232";
    zoom = "6";
    simpleModel = new DefaultMapModel();

    for (Sede s:sedes) {
      simpleModel.addOverlay(new Marker(new LatLng(Double.valueOf(s.getLongitud()),Double.valueOf(s.getLatitud())), s.getDireccion()));
    }
  }

  public List<Departament> getDepartaments() { return departaments; }
  public List<Provincia> getProvincias() { return provincias; }
  public List<Distrito> getDistritos() { return distritos; }
  public List<Persona> getPersonas() { return personas; }
  public List<Sede> getSedes() { return sedes; }
  public List<SedeVW> getSedesVW() { return sedesVW; }

  public String getIdSede() {
    return idSede;
  }

  public void setIdSede(String idSede) {
    this.idSede = idSede;
  }

  public void handleProvinciaChange() {
    provincias = provinciaService.getProvinciasByDepartamento(getIdDepartamento());
  }


  public void handleDistritoChange() {
    distritos = distritoService.getDistritoByDepartamentoAndProvincia(getIdDepartamento(), getIdProvincia());
  }

  public void handleSedeChange() {
    sedesVW = sedeService.findSedeByUbigeo(getIdDepartamento(), getIdProvincia(), getIdDistrito());
  }

  public Sede getSede() {
    return sede;
  }

  public void setSede(Sede sede) {
    this.sede = sede;
  }

  public Persona getPersona() {
    return persona;
  }

  public void setPersona(Persona persona) {
    this.persona = persona;
  }

  public MapModel getSimpleModel() {
    return simpleModel;
  }

  public void onRowEdit(RowEditEvent event) {
    Sede sede = (Sede) event.getObject();
    FacesMessage msg = new FacesMessage("Edición Registrada", String.valueOf(sede.getSede_id()));
    FacesContext.getCurrentInstance().addMessage(null, msg);
  }

  public void onRowCancel(RowEditEvent event) {
    Sede sede = (Sede) event.getObject();
    FacesMessage msg = new FacesMessage("Edición Cancelada", String.valueOf(sede.getSede_id()));
    FacesContext.getCurrentInstance().addMessage(null, msg);
  }

  public void onRowEditPersona(RowEditEvent event) {
    Persona persona = (Persona) event.getObject();
    FacesMessage msg = new FacesMessage("Edición Registrada", String.valueOf(persona.getPersonal_id()));
    FacesContext.getCurrentInstance().addMessage(null, msg);
  }

  public void onRowCancelPersona(RowEditEvent event) {
    Persona persona = (Persona) event.getObject();
    FacesMessage msg = new FacesMessage("Edición Cancelada", String.valueOf(persona.getPersonal_id()));
    FacesContext.getCurrentInstance().addMessage(null, msg);
  }

  public void create() throws IOException {
    sede.setCoddpto(idDepartamento);
    sede.setCodprov(idProvincia);
    sede.setCoddist(idDistrito);
    sedeService.createSede(sede);
    sedes = sedeService.allSedes();
  }

  public void createPersona() throws IOException {
    persona.setEstado(idEstado);
    personaService.createPersona(persona);
    personas = personaService.allPersona();
//    sedes = sedeService.allSedes();
  }


  public void buscarPersona() {
    personas = personaService.findPersonsByX(idDepartamento, idProvincia, idDistrito, idSede,nombres,apellidopaterno,apellidomaterno,idEstado);
  }

  public void limpiarPersona() {
    idDepartamento = null;
    idProvincia = null;
    idDistrito = null;
    nombres = null;
    apellidopaterno = null;
    apellidomaterno = null;
    provincias = null;
    distritos = null;
    personas = personaService.allPersona();
  }

  public void buscarSede() {
    longitudSuma = 0;
    latitudSuma = 0;
    sedes = sedeService.findSNDPSedeByUbigeo(idDepartamento, idProvincia, idDistrito);
    zoom = "8";
    simpleModel = new DefaultMapModel();

    for (Sede s:sedes) {
      longitudSuma += Double.valueOf(s.getLongitud());
      latitudSuma +=  Double.valueOf(s.getLatitud());
      contador = sedes.size();
      simpleModel.addOverlay(new Marker(new LatLng(Double.valueOf(s.getLongitud()),Double.valueOf(s.getLatitud())), s.getDireccion()));

    }
    longitud =  String.valueOf(longitudSuma/contador);
    latitud =  String.valueOf(latitudSuma/contador);
  }

  public void onMarkerSelect(OverlaySelectEvent event) {
      marker = (Marker) event.getOverlay();
  }
    public Marker getMarker() {
        return marker;
    }


  public void limpiarSede() {
    idDepartamento = null;
    idProvincia = null;
    idDistrito = null;
    sedes = sedeService.allSedes();
    longitud = "-10.08367";
    latitud = "-75.80232";
    zoom = "6";
    simpleModel = new DefaultMapModel();
    for (Sede s:sedes) {
    simpleModel.addOverlay(new Marker(new LatLng(Double.valueOf(s.getLongitud()),Double.valueOf(s.getLatitud())), s.getDireccion()));
    }
  }
}
