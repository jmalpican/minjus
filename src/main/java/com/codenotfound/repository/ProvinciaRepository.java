package com.codenotfound.repository;

import com.codenotfound.bean.Departament;
import com.codenotfound.bean.Provincia;
import com.codenotfound.mapper.DepartamentRowMapper;
import com.codenotfound.mapper.ProvinciaRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ProvinciaRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Provincia> allProvincias() {

        return jdbcTemplate.query("select * from ubigeo.VW_PROVINCIA ", new ProvinciaRowMapper());

    }


    public List<Provincia> getProvinciasByDepartamento(String idDepartamento) {
        return jdbcTemplate.query("select * from ubigeo.VW_PROVINCIA p where p.coddpto = ?", new ProvinciaRowMapper(), idDepartamento);
    }
}
