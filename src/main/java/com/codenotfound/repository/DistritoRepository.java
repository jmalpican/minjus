package com.codenotfound.repository;

import com.codenotfound.bean.Distrito;
import com.codenotfound.mapper.DistritoRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DistritoRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Distrito> allDistrito() {

        return jdbcTemplate.query("select * from ubigeo.VW_DISTRITO ", new DistritoRowMapper());

    }


    public List<Distrito> getDistritoByDepartamentoAndProvincia(String idDepartamento, String idProvincia) {
        return jdbcTemplate.query("select * from ubigeo.VW_DISTRITO p where p.coddpto = ? and p.codprov = ?", new Object[]{idDepartamento,idProvincia}, new DistritoRowMapper());
    }
}
