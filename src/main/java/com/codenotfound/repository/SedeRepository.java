package com.codenotfound.repository;

import com.codenotfound.bean.Persona;
import com.codenotfound.bean.Sede;
import com.codenotfound.bean.SedeVW;
import com.codenotfound.mapper.PersonaRowMapper;
import com.codenotfound.mapper.SedeRowMapper;
import com.codenotfound.mapper.SedeVWRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class SedeRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Sede> allSedes() {

        return jdbcTemplate.query("select * from ubigeo.sndp_sede", new SedeRowMapper());

    }

    public List<SedeVW> findSedesByUbigeo(String idDepartamento, String idProvincia, String idDistrito) {
        String sql = "select * from ubigeo.vw_sede p where 1=1 ";

        if (idDepartamento!=null && !idDepartamento.isEmpty()){
            sql = sql + " and p.coddpto = '".concat(idDepartamento).concat("' ");
        }
        if (idProvincia!=null && !idProvincia.isEmpty()){
            sql = sql + " and p.codprov = '".concat(idProvincia).concat("' ");
        }
        if (idDistrito!=null && !idDistrito.isEmpty()){
            sql = sql + " and p.coddist = '".concat(idDistrito).concat("' ");
        }
        return jdbcTemplate.query(sql, new SedeVWRowMapper());
    }

    public void createSede(Sede sede) {

        jdbcTemplate.update("INSERT INTO ubigeo.SNDP_SEDE(direccion, telefono, correo, coddpto, codprov, coddist, personal_id, longitud, latitud, activo, auditUsuarioCreacion, auditFechaCreacion) " +
                "VALUES('"+sede.getDireccion()+"','"+sede.getTelefono()+"','"+sede.getCorreo()+"','"+sede.getCoddpto()+"','"+sede.getCodprov()+"','"+sede.getCoddist()+"','"+sede.getPersonal_id()+"','"+sede.getLongitud()+"','"+sede.getLatitud()+"','"+1+"','"+"usuregis"+"','"+new Date() +"')");
    }

    public List<Sede> findSNDPSedeByUbigeo(String idDepartamento, String idProvincia, String idDistrito) {
        String sql = "select * from ubigeo.SNDP_SEDE p where 1=1 ";

        if (idDepartamento!=null && !idDepartamento.isEmpty()){
            sql = sql + " and p.coddpto = '".concat(idDepartamento).concat("' ");
        }
        if (idProvincia!=null && !idProvincia.isEmpty()){
            sql = sql + " and p.codprov = '".concat(idProvincia).concat("' ");
        }
        if (idDistrito!=null && !idDistrito.isEmpty()){
            sql = sql + " and p.coddist = '".concat(idDistrito).concat("' ");
        }
        return jdbcTemplate.query(sql, new SedeRowMapper());
    }
}
