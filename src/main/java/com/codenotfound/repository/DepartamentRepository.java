package com.codenotfound.repository;

import com.codenotfound.bean.Departament;
import com.codenotfound.mapper.DepartamentRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DepartamentRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Departament> allDepartament() {

        return jdbcTemplate.query("select * from ubigeo.vw_departamento", new DepartamentRowMapper());

    }


}
