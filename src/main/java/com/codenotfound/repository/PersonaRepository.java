package com.codenotfound.repository;

import com.codenotfound.bean.Persona;
import com.codenotfound.mapper.PersonaRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class PersonaRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Persona> allPersona() {

        return jdbcTemplate.query("select * from ubigeo.vw_listadopersonal", new PersonaRowMapper());

    }

    public void createPersona(Persona persona) {

        jdbcTemplate.update("INSERT INTO ubigeo.sndp_personal(apellido_paterno, apellido_materno, nombres, correo_electronico, estadoid, celular, nombredistritojudicial) " +
                "VALUES('"+persona.getApellido_paterno()+"','"+persona.getApellido_materno()+"','"+persona.getNombres()+"','"+persona.getCorreo_electronico()+"','"+persona.getEstado()+"','"+persona.getNumero_telefonico()+"','"+persona.getDireccion_distrital()+"')");
    }

    public List<Persona> findPersonsByX(String idDepartamento, String idProvincia, String idDistrito, String idSede, String nombres, String apellidopaterno, String apellidomaterno, String idEstado) {
        String sql = "select * from ubigeo.vw_listadopersonal p inner join ubigeo.vw_defensor_publico ON p.personal_id = ubigeo.vw_defensor_publico.personal_id where 1=1";

        if (idDepartamento!=null && !idDepartamento.isEmpty()){
            sql = sql + " and p.coddpto = '".concat(idDepartamento).concat("' ");
        }
        if (idProvincia!=null && !idProvincia.isEmpty()){
            sql = sql + " and p.codprov = '".concat(idProvincia).concat("' ");
        }
        if (idDistrito!=null && !idDistrito.isEmpty()){
            sql = sql + " and p.coddist = '".concat(idDistrito).concat("' ");
        }
        if (idSede!=null && !idSede.isEmpty()){
            sql = sql + " and p.sede_id = '".concat(idSede).concat("' ");
        }
        if (nombres!=null && !nombres.isEmpty()){
            sql = sql + " and p.nombres like '%".concat(nombres).concat("%' ");
        }
        if (apellidopaterno!=null && !apellidopaterno.isEmpty()){
            sql = sql + " and p.apellido_paterno like '%".concat(apellidopaterno).concat("%' ");
        }
        if (apellidomaterno!=null && !apellidomaterno.isEmpty()){
            sql = sql + " and p.apellido_materno like '%".concat(apellidomaterno).concat("%' ");
        }

        if (idEstado!=null && !idEstado.isEmpty()){
            sql = sql + " and p.estadoId = '".concat(idEstado).concat("' ");
        }
        return jdbcTemplate.query(sql, new PersonaRowMapper());
    }
}
