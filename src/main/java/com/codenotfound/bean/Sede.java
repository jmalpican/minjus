package com.codenotfound.bean;

import java.util.Date;

public class Sede {

    private String sede_id;
    private String direccion;
    private String telefono;
    private String correo;
    private String coddpto;
    private String codprov;
    private String coddist;
    private String nombrepto;
    private String nombreprov;
    private String nombredist;
    private String personal_id;
    private String longitud;
    private String latitud;
    private String longitudDpto;
    private String latitudDpto;
    private String activo;
    private String auditUsuarioCreacion;
    private Date auditFechaCreacion;
    private String auditUsuarioModifica;
    private Date auditFechaModifica;

    public String getNombreEstado() {
        return nombreEstado;
    }

    public void setNombreEstado(String nombreEstado) {
        this.nombreEstado = nombreEstado;
    }

    private String nombreEstado;

    public String getLongitudDpto() {
        return longitudDpto;
    }

    public void setLongitudDpto(String longitudDpto) {
        this.longitudDpto = longitudDpto;
    }

    public String getLatitudDpto() {
        return latitudDpto;
    }

    public void setLatitudDpto(String latitudDpto) {
        this.latitudDpto = latitudDpto;
    }
    public String getNombrepto() {
        return nombrepto;
    }

    public void setNombrepto(String nombrepto) {
        this.nombrepto = nombrepto;
    }

    public String getNombreprov() {
        return nombreprov;
    }

    public void setNombreprov(String nombreprov) {
        this.nombreprov = nombreprov;
    }

    public String getNombredist() {
        return nombredist;
    }

    public void setNombredist(String nombredist) {
        this.nombredist = nombredist;
    }

    public String getSede_id() {
        return sede_id;
    }

    public void setSede_id(String sede_id) {
        this.sede_id = sede_id;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCoddpto() {
        return coddpto;
    }

    public void setCoddpto(String coddpto) {
        this.coddpto = coddpto;
    }

    public String getCodprov() {
        return codprov;
    }

    public void setCodprov(String codprov) {
        this.codprov = codprov;
    }

    public String getCoddist() {
        return coddist;
    }

    public void setCoddist(String coddist) {
        this.coddist = coddist;
    }

    public String getPersonal_id() {
        return personal_id;
    }

    public void setPersonal_id(String personal_id) {
        this.personal_id = personal_id;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getAuditUsuarioCreacion() {
        return auditUsuarioCreacion;
    }

    public void setAuditUsuarioCreacion(String auditUsuarioCreacion) {
        this.auditUsuarioCreacion = auditUsuarioCreacion;
    }

    public Date getAuditFechaCreacion() {
        return auditFechaCreacion;
    }

    public void setAuditFechaCreacion(Date auditFechaCreacion) {
        this.auditFechaCreacion = auditFechaCreacion;
    }

    public String getAuditUsuarioModifica() {
        return auditUsuarioModifica;
    }

    public void setAuditUsuarioModifica(String auditUsuarioModifica) {
        this.auditUsuarioModifica = auditUsuarioModifica;
    }

    public Date getAuditFechaModifica() {
        return auditFechaModifica;
    }

    public void setAuditFechaModifica(Date auditFechaModifica) {
        this.auditFechaModifica = auditFechaModifica;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
}
