package com.codenotfound.bean;

import java.util.Date;

public class SedeVW {

    private String sede_id;
    private String distritojudicial_id;
    private String coddpto;
    private String codprov;
    private String coddist;
    private String nombre;

    public String getSede_id() {
        return sede_id;
    }

    public void setSede_id(String sede_id) {
        this.sede_id = sede_id;
    }

    public String getDistritojudicial_id() {
        return distritojudicial_id;
    }

    public void setDistritojudicial_id(String distritojudicial_id) {
        this.distritojudicial_id = distritojudicial_id;
    }

    public String getCoddpto() {
        return coddpto;
    }

    public void setCoddpto(String coddpto) {
        this.coddpto = coddpto;
    }

    public String getCodprov() {
        return codprov;
    }

    public void setCodprov(String codprov) {
        this.codprov = codprov;
    }

    public String getCoddist() {
        return coddist;
    }

    public void setCoddist(String coddist) {
        this.coddist = coddist;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
