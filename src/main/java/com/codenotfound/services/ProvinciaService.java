package com.codenotfound.services;


import com.codenotfound.bean.Departament;
import com.codenotfound.bean.Provincia;

import java.util.List;

public interface ProvinciaService {
    List<Provincia> allProvincias();
    List<Provincia> getProvinciasByDepartamento(String idDepartamento);
}
