package com.codenotfound.services;


import com.codenotfound.bean.Departament;

import java.util.List;

public interface DepartamentService {
    List<Departament> allDepartament();
}
