package com.codenotfound.services;

import com.codenotfound.bean.Sede;
import com.codenotfound.bean.SedeVW;

import java.util.List;

public interface SedeService {
    List<Sede> allSedes();
    void createSede(Sede sede);

    List<SedeVW> findSedeByUbigeo(String idDepartamento, String idProvincia, String idDistrito);

    List<Sede> findSNDPSedeByUbigeo(String idDepartamento, String idProvincia, String idDistrito);
}
