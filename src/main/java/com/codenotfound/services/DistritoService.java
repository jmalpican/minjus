package com.codenotfound.services;


import com.codenotfound.bean.Distrito;

import java.util.List;

public interface DistritoService {
    List<Distrito> allDistrito();
    List<Distrito> getDistritoByDepartamentoAndProvincia(String idDepartamento, String idProvincia);
}
