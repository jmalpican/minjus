package com.codenotfound.services;

import com.codenotfound.bean.Persona;


import java.util.List;

public interface PersonaService {
    List<Persona> allPersona();
    void createPersona(Persona persona);
    List<Persona> findPersonsByX(String idDepartamento, String idProvincia, String idDistrito, String idSede, String nombres, String apellidopaterno, String apellidomaterno, String idEstado);
}
