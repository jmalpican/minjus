package com.codenotfound.services.impl;

import com.codenotfound.bean.Sede;
import com.codenotfound.bean.SedeVW;
import com.codenotfound.repository.SedeRepository;
import com.codenotfound.services.SedeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SedeServiceImpl implements SedeService{

    @Autowired
    private SedeRepository sedeRepository;

    public List<Sede> allSedes() {
        return sedeRepository.allSedes();
    }

    public void createSede(Sede sede) {
        sedeRepository.createSede(sede);
    }

    @Override
    public List<SedeVW> findSedeByUbigeo(String idDepartamento, String idProvincia, String idDistrito) {
        return sedeRepository.findSedesByUbigeo(idDepartamento, idProvincia, idDistrito);
    }

    @Override
    public List<Sede> findSNDPSedeByUbigeo(String idDepartamento, String idProvincia, String idDistrito) {
        return sedeRepository.findSNDPSedeByUbigeo(idDepartamento, idProvincia, idDistrito);
    }
}
