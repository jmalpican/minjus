package com.codenotfound.services.impl;

import com.codenotfound.bean.Distrito;
import com.codenotfound.bean.Provincia;
import com.codenotfound.repository.DistritoRepository;
import com.codenotfound.services.DistritoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class DistritoServiceImpl implements DistritoService {

    @Autowired
    private DistritoRepository distritoRepository;

    @Override
    public List<Distrito> allDistrito() {
        return null;
    }

    @Override
    public List<Distrito> getDistritoByDepartamentoAndProvincia(String idDepartamento, String idProvincia) {

        return distritoRepository.getDistritoByDepartamentoAndProvincia(idDepartamento,idProvincia);
    }
}
