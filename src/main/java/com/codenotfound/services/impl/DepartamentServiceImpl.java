package com.codenotfound.services.impl;

import com.codenotfound.bean.Departament;
import com.codenotfound.repository.DepartamentRepository;
import com.codenotfound.services.DepartamentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class DepartamentServiceImpl implements DepartamentService {

    @Autowired
    private DepartamentRepository departamentRepository;

    public List<Departament> allDepartament() {
        return departamentRepository.allDepartament();
    }
}
