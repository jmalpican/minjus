package com.codenotfound.services.impl;

import com.codenotfound.bean.Persona;
import com.codenotfound.repository.PersonaRepository;
import com.codenotfound.services.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonaServiceImpl implements PersonaService {

    @Autowired
    private PersonaRepository personaRepository;

    public List<Persona> allPersona() {
        return personaRepository.allPersona();
    }

    public void createPersona(Persona persona) {
        personaRepository.createPersona(persona);
    }

    @Override
    public List<Persona> findPersonsByX(String idDepartamento, String idProvincia, String idDistrito, String idSede, String nombres, String apellidopaterno, String apellidomaterno, String idEstado) {
        return personaRepository.findPersonsByX(idDepartamento, idProvincia, idDistrito, idSede, nombres,apellidopaterno,apellidomaterno, idEstado);
    }
}
