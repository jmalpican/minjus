package com.codenotfound.services.impl;

import com.codenotfound.bean.Provincia;
import com.codenotfound.repository.ProvinciaRepository;
import com.codenotfound.services.ProvinciaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ProvinciaServiceImpl implements ProvinciaService {

    @Autowired
    private ProvinciaRepository provinciaRepository;

    @Override
    public List<Provincia> allProvincias() {
        return null;
    }

    @Override
    public List<Provincia> getProvinciasByDepartamento(String idDepartamento) {
        return provinciaRepository.getProvinciasByDepartamento(idDepartamento);
    }
}
